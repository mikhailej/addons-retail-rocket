# Module: Retail Rocket #

### In detail ###

* Designed for cs cart
* Version module 1.0

### How to install and configure the module? ###

* To install this module, copy all the files to / cs-cart / with the folder structure preserved.
* Install the module on the "Modules" page.